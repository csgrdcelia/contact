<?php

return [
    'propel' => [
        'database' => [
            'connections' => [
                'contact' => [
                    'adapter'    => 'mysql',
                    'classname'  => 'Propel\Runtime\Connection\ConnectionWrapper',
                    'dsn'        => 'mysql:host=localhost;dbname=contact',
                    'user'       => 'root',
                    'password'   => 'root',
                    'attributes' => []
                ]
            ]
        ],
                'general' => [
            'project' => 'contact'
        ],
        'paths' => [
            'projectDir' => '/var/www/html/contact',
            'schemaDir' => '/var/www/html/contact/script',
            'phpDir' => '/var/www/html/contact/module/Contact/src'
        ],
        'runtime' => [
            'defaultConnection' => 'contact',
            'connections' => ['contact']
        ],
        'generator' => [
            'defaultConnection' => 'contact',
            'connections' => ['contact']
        ]
    ]
];
